void main(){
  List<String> pastWinners = [
    "FNB Banking app",
    "SnapScan",
    "Live Inspect",
    "WumDrop",
    "Domestly",
    "Shyft",
    "Khula Ecosystem",
    "Naked Insurance",
    "Easy Equities",
    "Ambani Afrika"
  ];
 
  pastWinners.sort(); /*a. Sorting the apps, by default, in ascending order */
  print("winning apps from 2012 to 2021 Sorted: $pastWinners\n");

  /*b. Printing winning app of 2017 and 2018 */
  String twentySeventeen = pastWinners[7];
  String twentyEighteen = pastWinners[4];
  print("Winning app of 2017 = $twentySeventeen \nWinning app of 2018 = $twentyEighteen\n");

  /*c. Getting the size of the array*/
  int arraySize = pastWinners.length;
  print("The total number of apps in the array = $arraySize");





}