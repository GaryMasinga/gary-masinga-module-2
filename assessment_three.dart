class AppOfTheYear {
  String? name;
  String? category;
  String? developer;
  int? year;

  String lowerCase(String? n){
    return n!.toLowerCase();
  }

  showAppOfTheYearInfo(){
    print("App name: " + lowerCase(name));
    print("Category is: ${category}");
    print("The developer of the app is: $developer");
    print("Year of winning is: ${year}");
  }

}

void main() {
  var object = new AppOfTheYear();
  object.name = "Easy Equities";
  object.category = "Best Consumer Solution";
  object.developer = "First World Trader";
  object.year = 2020;

  object.showAppOfTheYearInfo();
  
}